{ description = "Extremely basic flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    twin = {
      url = "git+https://forge.antimattercloud.nl/Egg/twin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { self, nixpkgs, flake-utils, twin }:
  flake-utils.lib.eachDefaultSystem (system:
  let pkgs = import nixpkgs { inherit system; };
  in {
    packages = rec {
      voidgem = pkgs.stdenvNoCC.mkDerivation {
        pname = "voidgem";
        version = "1.1";
        src = ./voidcruiser;

        nativeBuildInputs = [
          twin.packages.${system}.default
        ];

        buildPhase = ''
          twin -d log -nrb index.gmi --filename -t Gemlog > log.gmi
        '';

        installPhase = ''
          mkdir -p $out/share/
          cp -r . $out/share/
        '';
      };
    };
  });
}
