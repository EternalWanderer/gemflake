# Story Ideas

These a few ideas for stories I have had at some point but don't know what to do with. Since my head is as much of a mess as it is, there has to be something here someone could make something interesting with.

Everything on this page is 
```CC0
CC0
```

* A vampire that wants to be able to eat garlic again, it being the one thing they miss about being human.

* Having to pay for the amount of words you can speak in a day; in other words, word quotas.

* Having a limited amount of words in a lifespan.

* Musician that plays avalanches instead of traditional instruments (think Ungraven's Aggro Master, especially the intro).
