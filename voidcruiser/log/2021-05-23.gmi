# Thoughts About Pads

So I got myself a Lenovo Thinkpad T440p the other day. In terms of usability this is a huge step up from my HP Omen 15something - the one with the GTX 1060. Why a T440p you may ask? Besides my Omen, I had been using a Dell Latitude E5500 and wanted something quite a bit stronger than it and more repairable then my Omen. The second criterium was quite easy to meet. Omen laptops are a nightmare to perform any kind of service on. Even just swapping the HDD took me around half an hour. 

Mind you, I only really developed an interest in hardware since around a year ago. So I don't really know if a T440p was the best choice for my situation. Either way though. I am really happy with it. Compared to the Omen, it is a lot quieter, lighter, has better battery life and because it is smaller generally nicer to have with me. When it comes to sheer performance however, the Omen wins with it's 12 4.100GHz cores and 144Hz screen. For anything besides heavy compilation tasks and gaming however, it really isn't particularly usable. In terms of battery life, I was astonished I could get about an hour out of it when using my AwesomeWM setup on Linux. Back on Windows I never dared to leave it without a charger for longer than half an hour. But then NVidia's throttling of performance when not hooked up to a charger definitely didn't help with that either. Another big problem, especially on Windows, is the ever-present noise of the rather loud fans. On Linux they are often calmly spinning, only fully firing up when doing a heavier task like playing a game at 144fps or having >20 tabs open in a chromium based browser. On Windows, however, they rarely go below "jet engine" levels of noise, even when just booting into it.

I've had to send the Omen off on repair trips five times by now as well. Three of which were because of the company where I got it having really bad customer service. The remaining two had to do with the fans getting stuck in the protective shields of the fans. The reason being the that damn thing easily reaches temperatures capable of warping the metal bits. 
The other time was because of a broken GPU. When I called them about it, it turned out they had switched the entire motherboard because the GPU is soldered to the motherboard itself.

Then there are other problems like not having a dedicated home key, instead HP opted for an omen-logo button that opens up the useless proprietary Omen control panel thing on Windows. If you even have it installed that is. If you don't, you're stuck with a useless key that does nothing on it's own. In order for it to act like a home key, you have the press the fn key along with it. Why? Why is this is a thing HP?
Meanwhile the only problems I have with the T440p's keyboard are the positions of the left control and fn keys being swapped compared to what I'm used to and the backspace key sometimes scraping along the chassis a little if pressed at an awkward angle.

The next step I think I'm gonna take would be getting a docking station for the T440p as docking stations are neat.
Other than that, I can see myself picking up an x200 at some point as well, but I don't know how similar that would be compared to my Latitude in terms of performance.

=> /images/rice.webp Obligatory rice screenshot

=> ../log.gmi Back
