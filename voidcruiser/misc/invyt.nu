#!/usr/bin/env nu

def main [url: string] {
	let parsed_url = $url | url parse

	let yt_url = if ($parsed_url.path | str contains '/watch') {
		if $parsed_url.host != "youtube.com" {
			$url | str replace $parsed_url.host "youtube.com"
		} else {
			$url
		}
	} else {
		error make {
			msg: "Not a YouTube/Invidious/Piped URL"
			label: {
				text: "Expects a YouTube/Invidious/Piped URL"
				span: (metadata $url).span
			}
		}
	}

	yt-dlp --embed-chapters --embed-metadata --embed-subs $yt_url
}
